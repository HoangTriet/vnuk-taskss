package vn.edu.vnuk.tasks.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class Sql_0010_CreateTasks {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0010_CreateTasks(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "create table tasks ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "description VARCHAR(255), "
						+ "achieved BOOLEAN, "
						+ "date_of_achievement DATE, "
						+ "primary key (id)"
						+ ");";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("New table in DB !");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		        connection.close();
		}
		
	}

}
